###############################################################################
# FILENAME: xp.rb
# DESCRIPTION:
# Extension to Asciidoctor-PDF which converts the experience block into a more
# resume-friendly format. This is an extension used for convenience as now all
# that must be done when writing the experience block is to surround it with
# the tags '[xp]' and  '[/xp]'.
###############################################################################

require 'asciidoctor'
require 'asciidoctor/extensions'


include ::Asciidoctor

Extensions.register do
  include_processor Xp
end

class Xp < Extensions::IncludeProcessor

  # Styling Format
  XpFormat = "[cols=\"6,2\", align=center, options=\"noheader\"]\n"
  XpTab = "|===\n"

  XpAsciidocFilename = 'experience.adoc'
  XpBlock = '[xp]'
  XpBlockEnd = '[/xp]'
  XpTagDate = 'date: '
  XpTagJob = 'job: '
  XpTagOrg = 'org: '
  XpTagLoc = 'loc: '

  def process doc, reader, target, attr
    newLines = []
    desc = []
    inXpBlock = false
    content = (open target).readlines

    # If we are processing an experience file, run through these steps to parse
    # the tags and write to a tabular format
    if target.include? XpAsciidocFilename
      newLines.push(XpFormat)
      newLines.push(XpTab)
      for line in content do
        if line.start_with?(XpBlock)
          inXpBlock = true
          date = ""
          job = ""
          org = ""
          loc = ""
          desc = []
          next
        elsif line.start_with?(XpBlockEnd)
          inXpBlock = false

          # Construct the Table
          newLines.push("<.^| " + job + "\n")
          newLines.push(">.^| " + date + " \n\n")

          newLines.push("<.^| " + org + "\n")
          newLines.push(">.^| " + loc + " \n\n")

          # Add 2 empty rows for content separation
          newLines.push("2+|\n")
          newLines.push("2+|\n")

          # Description text must be formatted as Asciidoc
          newLines.push("2+a| \n")

          # Copy in the remaining description lines
          for line in desc
            newLines.push(line)
          end

          # Finish the table
          newLines.push("\n")
          next
        end

        if inXpBlock
          if line.start_with?(XpTagDate)
            line.slice! XpTagDate
            date = line.chomp
          elsif line.start_with?(XpTagJob)
            line.slice! XpTagJob
            job = line.chomp
          elsif line.start_with?(XpTagOrg)
            line.slice! XpTagOrg
            org = line.chomp
          elsif line.start_with?(XpTagLoc)
            line.slice! XpTagLoc
            loc = line.chomp
          else
            desc.push(line)
          end
        end
      end
      # Complete table tag and render
      newLines.push(XpTab)
      reader.push_include newLines, target, target, 1, attr
    else
      # In this case, we're not including that file, so just pass the content
      reader.push_include content, target, target, 1, attr
    end

    # Process the lines
    reader
  end
end
